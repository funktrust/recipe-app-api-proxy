FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="maintainer@philvadala.com"

##Copy files required for Nginx
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

##Default environment variables for Docker container
ENV LISTEN_PORT=8000
ENV APP_HOST=app
ENV APP_PORT=9000

##Login as root
USER root
##Creates new directory
RUN mkdir -p /vol/static
##Set folder permissions to serve static files from this directory
RUN chmod 755 /vol/static
##Creates empty file
RUN touch /etc/nginx/conf.d/default.conf
##Changes ownership to nginx
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

##Copy entrypoint file
COPY ./entrypoint.sh /entrypoint.sh
##Set entrypoint as executable
RUN chmod +x /entrypoint.sh

##Login as user Nginx
USER nginx

##Run Nginx
CMD ["/entrypoint.sh"]